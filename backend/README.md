# Tic-Tac-Toe React App

A strategic Tic-Tac-Toe game built with React that features an AI opponent using the Minimax algorithm. Players can also save and view top scores through a backend API.

## Why Minimax on the Front-end?

For this application, the Minimax algorithm was implemented on the front-end primarily for performance and immediacy. Given the relatively small game tree of Tic-Tac-Toe, running the algorithm directly on the client-side ensures that moves are calculated quickly without the latency that might arise from server-side computation. This provides a smoother, near-instantaneous response for users, making the gameplay more engaging.

## Features

1. **Minimax Algorithm** - The computer opponent uses the Minimax algorithm to determine its moves, making it almost impossible to beat.
2. **Scoreboard** - Players can save their scores and view the top scores.
3. **Interactive UI** - Built with React and styled components for an interactive gaming experience.

## Setup & Installation

### Prerequisites

- Node.js and npm installed on your machine. (You can download them from [here](https://nodejs.org/))
- An active backend endpoint for saving and fetching scores (e.g., `http://localhost:8000/api`). This README assumes you have a server running on `http://localhost:8000/`.
- XAMPP for database management.

### Steps

1. **Clone the repository**
git clone https://gitlab.com/DavhanaMuvhuso/tic-tac-toe.git
cd tic-tac-toe-react-app


2. **Install dependencies**

npm install


3. **Set up the backend endpoint**
In the root of the project, create a `.env` file and specify the backend endpoint:

REACT_APP_BACKEND_URL=http://localhost:8000/api

Replace `http://localhost:8000/api` if your backend is hosted elsewhere.

4. **XAMPP Installation and Database Setup**:

   - Download & Install XAMPP from [here](https://www.apachefriends.org/index.html) and install it with Apache and MariaDB components.
   
   - Start Apache and MariaDB via the XAMPP control panel.
   
   - Access [phpMyAdmin](http://localhost/phpmyadmin) and create a new database (e.g., "tic_tac_toe"). If required, import any `.sql` file provided.

   Ensure your backend application connects to the database properly.

5. **Start the development server**

npm start

This command runs the app in development mode. Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## How to Play

1. **Enter Your Name** - Before starting, input your name.
2. **Making Moves** - Click on any of the Tic-Tac-Toe board squares to make your move.
3. **AI Opponent** - After your move, the computer will make its move.
4. **End of Game** - The game ends when either player gets three in a row or all squares are filled.
5. **Save Score** - At the end of the game, your score will be saved if you provided a name.

## Contributing

We welcome contributions! If you find a bug or have a feature request, please open an issue. If you wish to contribute code, please fork this repository and submit a pull request.

---