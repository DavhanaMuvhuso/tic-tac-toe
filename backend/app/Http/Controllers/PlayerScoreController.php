<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PlayerScore;  // Assuming you have a PlayerScore model.

class PlayerScoreController extends Controller
{
    // Store a player's score.
    public function store(Request $request)
    {
        $request->validate([
            'player_name' => 'required|string|max:255',
            'score' => 'required|integer',
        ]);

        $score = new PlayerScore();
        $score->player_name = $request->player_name;
        $score->score = $request->score;
        $score->save();

        return response()->json(['message' => 'Score saved successfully!', 'data' => $score], 201);
    }

    // Retrieve the top 10 scores.
    public function topScores()
    {
        $scores = PlayerScore::orderBy('score', 'desc')->take(10)->get();
        return response()->json($scores);
    }
}
