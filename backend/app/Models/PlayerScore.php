<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlayerScore extends Model
{
    use HasFactory;

    protected $table = 'player_scores';  // optional, just for clarity
    protected $fillable = ['player_name', 'score'];
}
